﻿using FytSoa.Common;
using FytSoa.Core.Model.Cms;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FytSoa.Service.Interfaces
{
    /// <summary>
    /// 图片管理业务接口
    /// </summary>
    public interface ICmsImageService : IBaseServer<CmsImage>
    {
        
    }
}
