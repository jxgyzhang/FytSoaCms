# FytSoaCms

#### 项目介绍
CMS内容管理系统  
使用NetCore2.1开发，  Vs2017       数据Mysql  
系统管理  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;权限管理  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;站点管理  
内容管理  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;栏目管理  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;模板管理  
扩展管理   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;广告管理  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;留言管理    

权限管理目前还没有做到具体功能模块，只做到菜单模块  
后续不断完善权限管理  
  
其它功能模块也在后续不断增加，投票系统，下载系统，微信公众号模块等  
  
后台地址：http://localhost:4909/fytadmin  
账号：admins   密码：123456

#### 软件架构
0. 前端框架使用Layui Vue
1. DB=数据库文件夹  mysql
2. FytSoa.Api=webApi  可在项目中配置使用权限，如后台管理，APP,微信等
3. FytSoa.Common=公共类，提供项目一些常用工具方法
4. FytSoa.Core=数据库实体对象，以及连接对象
5. FytSoa.Extensions=扩展方法
6. FytSoa.Service=业务类，接口和实现       提供代码生成器
7. FytSoa.Web=项目目录，Jwt认证  Swagger可视化接口服务

#### 安装教程

1. 开发工具   visual studio 2017  5.3+
2. 数据库     Mysql 8.0.12
3. NetCore 2.1
4. Orm使用的SqlSugar   网址：http://www.codeisbug.com
5. 文件存储使用的七牛云，在FytSoa.Extensions  项目中，需要配置在七牛云申请的AK、SK   具体请看七牛云开发文档


#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 有问题联系我

1. QQ群：86594082，  726648662，225982985
2. 群内提供，代码生成工具，欢迎大家踊跃加群


### 项目截图预览

![](http://img.feiyit.com/feiyit/website/else/login.png)
![](http://img.feiyit.com/feiyit/website/else/2.png)
![](http://img.feiyit.com/feiyit/website/else/3.png)
![](http://img.feiyit.com/feiyit/website/else/4.png)
![](http://img.feiyit.com/feiyit/website/else/5.png)
![](http://img.feiyit.com/feiyit/website/else/6.png)
![](http://img.feiyit.com/feiyit/website/else/7.png)
![](http://img.feiyit.com/feiyit/website/else/8.png)